# Employees Management

## Laravel Configuration Guide

Use the composer manager to install Laravel. 

```bash
cd employee-mnk-re && composer install
```


## Database
Create database and add database and root and passsword name as shown in below.
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=mnk-re
DB_USERNAME=root
DB_PASSWORD=
```
Once database configutation is done then after run migration command to create tables.

```bash
cd employee-mnk-re
php artisan migrate:fresh --seed - This will create tables plus 20 employees records.
```

## Run Server (Laravel)
```bash
// Laravel
cd employee-mnk-re
php artisan serve

```
## URL
```
Reports: http://127.0.0.1:8000/reports
Search: http://127.0.0.1:8000/search # Search Page will take you to employee details page.
Employee Details: http://127.0.0.1:8000/employee/1
```


## Author
Chiragkumar Patel

