<?php

namespace App\Http\Controllers;

use App\Repositories\IEmployeeRepositoryInterface;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

/**
 * @author Chiragkumar Patel
 */
class EmployeesController extends Controller
{
    public const STATUS = [0 => 'Active', 1 => 'Resigned', 2 => 'Terminated', 3 => 'Deactivate'];
    /**
     * @var IEmployeeRepositoryInterface
     */
    private IEmployeeRepositoryInterface $employee_repository;

    /**
     * EmployeeController constructor.
     * @param IEmployeeRepositoryInterface $employee_repository
     */
    public function __construct(IEmployeeRepositoryInterface $employee_repository)
    {
        $this->employee_repository = $employee_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): View
    {
        return view('search');
    }

    /**
     * Display Search page
     * @param $terms
     * @return JsonResponse
     */
    public function searchEmployee($terms): JsonResponse
    {
        if(empty($terms)){
            return \response()->json(['error' => 'terms parameter is required.'], 400);
        }
        try {
            $employees = $this->employee_repository->search($terms);
            return \response()->json(['employees' => $employees]);
        } catch (Exception $e) {
            // Handle the exception and return a meaningful error message to the user
            return \response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Display employee edit page
     * @param $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function show($id)
    {
        if (!is_numeric($id)) {
            return redirect()->to('search')->with('error', 'Invalid employee ID');
        }
        try {
            $employee = $this->employee_repository->getById($id);
            if(empty($employee)){
                return redirect()->to('search')->with('error', 'Employee not found');
            }
            return \view('edit', ['employee' => $employee, 'options' => self::STATUS]);
        } catch (\Exception $e) {
            // Handle the exception and redirect to the search page with an error message
            Log::error($e->getMessage());
            return redirect()->to('search')->with('error', 'An error has occurred while processing your request.');
        }
    }

    /**
     * Editing the employee information
     * @param Request $request
     * @param $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit(Request $request, $id)
    {
        $validatedData = $request->validate([
            'joining_date' => 'required|date',
            'status' => 'required'
        ]);
        try {
            $this->employee_repository->edit($validatedData, $id);
        } catch (\Exception $e) {
            // Log the error or return a custom error message
            return back()->withErrors($e->getMessage());
        }
        $employee = $this->employee_repository->getById($id);
        return view('edit', ['employee' => $employee, 'options' => self::STATUS]);
    }

    /**
     * Get reports of all employees
     * @return JsonResponse
     */
    public function getEmployeesData(Request $request): JsonResponse
    {
        $year = $request->input('year-filter');
        $minSalary = $request->input('min-salary-filter');
        $maxSalary = $request->input('max-salary-filter');
        $employees = $this->employee_repository->reports($year, $minSalary, $maxSalary);
        $options = self::STATUS;

        return Datatables::of($employees)
            ->addColumn('status', function ($employee) use ($options) {
                return $options[$employee->status];
            })
            ->make(true);
    }

    /**
     * Display the reports of employee with salary and salary range filter
     * @return View
     */
    public function reports(): View
    {
        return \view('reports.report');
    }
}
