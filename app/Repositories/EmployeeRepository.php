<?php

namespace App\Repositories;

use App\Models\Employees;

/**
 * @author Chiragkumar Patel
 */
class EmployeeRepository implements IEmployeeRepositoryInterface
{

    /**
     * @var Employees
     */
    private $model;

    public function __construct(Employees $model)
    {
        $this->model = $model;
    }

    /** get all employee data
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->select("*")->get();
    }

    /**
     * Search the employee information using employee id or first_name
     * @param $terms
     * @return mixed
     */
    public function search($terms)
    {
        // TODO: Implement search() method.
        $employee_ids = $this->model->select("*")->whereIn('employee_id', [$terms])->get();
        if (empty($employee_ids->count())) {
            return $this->model->select("*")->where('first_name', 'LIKE', "%$terms%")->get();
        } else {
            return $employee_ids;
        }
    }

    /**
     * Get information of employee by employee_id
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->select('*')->where('employee_id', $id)->first();
    }


    /**
     * Edit employee details
     * @param array $attributes
     * @return mixed | boolean
     */
    public function edit(array $attributes, $id)
    {
        return $this->model->where('employee_id', $id)->update($attributes);
    }


    /**
     * Display reports using min, max salary with year
     * @param $year
     * @param $minSalary
     * @param $maxSalary
     * @return mixed
     */
    public function reports($year = null, $minSalary = null, $maxSalary = null)
    {
        $query = $this->model->select("*");
        if (!empty($year)) {
            $query->whereYear('joining_date', $year);
        }
        if (!empty($minSalary) && empty($maxSalary)) {
            $query->where('salary', '>=', $minSalary);
        } elseif (empty($minSalary) && !empty($maxSalary)) {
            $query->where('salary', '<=', $maxSalary);
        } elseif (!empty($minSalary) && !empty($maxSalary))  {
            $query->whereBetween('salary', [$minSalary, $maxSalary]);
        }
        $result = $query->get();
        return $result;
    }
}
