<?php

namespace App\Repositories;

/**
 * @author Chiragkumar Patel
 */
interface IEmployeeRepositoryInterface
{
    public function getAll();

    public function search($terms);

    public function getById($id);

    public function edit(array $attributes, $id);

    public function reports($year = null, $minSalary = null, $maxSalary = null);
}
