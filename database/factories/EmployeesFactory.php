<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Employees>
 */
class EmployeesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'employee_id' => fake()->unique()->numberBetween(1,20),
            'first_name' => fake()->firstName,
            'last_name' => fake()->lastName,
            'DOB' => fake()->date,
            'salary' => fake()->numberBetween(10000, 500000),
            'joining_date' => fake()->date(),
            'status' => fake()->numberBetween(0, 3)
        ];
    }
}
