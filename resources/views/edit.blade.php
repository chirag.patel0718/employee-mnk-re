@extends('layouts/layout')

@section('content')

    <div class="col-md-7 col-lg-8">
        <h4 class="mb-3">Employee Details:</h4>
        <form action="{{ route('edit', $employee->employee_id) }}" method="post" class="employee_form bootstrap-iso" novalidate="">
            @csrf
            <div class="row g-3">
                <div class="col-sm-6">
                    <label for="firstName" class="form-label">First name</label>
                    <input readonly type="text" class="form-control" id="firstName" placeholder=""
                           value="{{ $employee->first_name }}">
                </div>

                <div class="col-sm-6">
                    <label for="lastName" class="form-label">Last name</label>
                    <input readonly type="text" class="form-control" id="lastName" placeholder=""
                           value="{{ $employee->last_name }}">
                </div>


                <div class="col-6">
                    <label for="dob" class="form-label">DOB</label>
                    <input readonly value="{{ $employee->DOB }}" type="text" class="form-control" id="dob"
                           placeholder="Date"
                    >
                </div>

                <div class="col-6">
                    <label for="salary" class="form-label">Salary</label>
                    <input readonly class="form-control" id="salary" type="text" value="{{ $employee->salary }}"
                           name="salary">
                </div>

                <div class="col-6">
                        <label for="joining_date" class="form-label">Joining Date</label>
                        <input name="joining_date" data-provide="datepicker" value="{{ $employee->joining_date }}" type="text" class="form-control" id="joining_date"
                               placeholder="Date"
                               required="">
                        <div class="invalid-feedback">
                            Please select a joining date.
                        </div>

                </div>

                <div class="col-6">
                    <label for="status" class="form-label">Status</label>
                    <select required="" class="form-control" id="status" name="status">
                        <option value="">Select Status</option>
                        @foreach ($options as $key => $value)
                            <option value="{{ $key }}"
                                    @if ($key == old('status', $employee->status))
                                        selected="selected"
                                @endif
                            >{{ $value }}</option>
                        @endforeach
                    </select>

                    <div class="invalid-feedback">
                        Please select a valid country.
                    </div>
                </div>
                <hr class="my-4">
            </div>
            <button class="w-100 btn btn-primary btn-lg" type="submit">Edit</button>
        </form>
    </div>

@endsection

@section('custom_script')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
        $('#joining_date').datepicker({
            format: 'yyyy-mm-dd',
            defaultViewDate: {year: '2014'},
            autoclose: true,
            endDate: '+0d',
        });
    </script>
@endsection
