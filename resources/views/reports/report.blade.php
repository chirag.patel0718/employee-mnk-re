@extends('layouts/layout')

@section('content')
    <label>Search by Year:</label>


    <form id="filter-form">
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label for="year-filter">Select Year</label>
                    <select class="form-control" id="year-filter">
                        <option value="">Select Year</option>
                        @for($i = 1950; $i <= 2023; $i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="min-salary-filter">Minimum Salary</label>
                    <input type="number" class="form-control" id="min-salary-filter">
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="max-salary-filter">Maximum Salary</label>
                    <input type="number" class="form-control" id="max-salary-filter">
                </div>
            </div>
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <br>
    <table id="employees-table" class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Employee Id</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">DOB</th>
            <th scope="col">Salary</th>
            <th scope="col">Joining Date</th>
            <th scope="col">Status</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

@endsection

@section('custom_script')
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>


    <script type="text/javascript">
        $('#filter-form').submit(function (event) {
            event.preventDefault();
            var year = $('#year-filter').val();
            var minSalary = $('#min-salary-filter').val();
            var maxSalary = $('#max-salary-filter').val();

            $('#employees-table').DataTable().ajax.url('/getEmployeesData?year-filter=' + year + '&min-salary-filter=' + minSalary + '&max-salary-filter=' + maxSalary).load();
        });
        $(document).ready(function () {
            $('#employees-table').DataTable({
                processing: true,
                serverSide: true,
                "ajax": {
                    "url": "/getEmployeesData",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        "_token": "{{ csrf_token() }}"
                    }
                },
                dom: 'Bfrtip',
                buttons: [
                    'pdf', 'excel'
                ],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'employee_id', name: 'employee_id'},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'DOB', name: 'DOB'},
                    {data: 'salary', name: 'salary'},
                    {data: 'joining_date', name: 'joining_date'},
                    {data: 'status', name: 'status'},
                ]
            });
        });
    </script>

@endsection
