@extends('layouts/layout')

@section('content')
    <br>
    <div class="container">
        <div class="row">
            <label>Search Employee
                <input id="tags" type="text" placeholder="search" value="" name="search">
            </label>
        </div>
    </div>
@endsection

@section('custom_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tags").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: "/search-employee/" + request.term,
                        success: function (data) {
                            var transformed = $.map(data.employees, function (el) {
                                return {
                                    value: el.first_name,
                                    url: `/employee/${el.employee_id}`,
                                };
                            });
                            response(transformed);
                        },
                        error: function () {
                            response([]);
                        }
                    });
                },
                select: function (event, ui) {
                    console.log(ui)
                    window.location = ui.item.url;
                }
            });
        });
    </script>
@endsection
