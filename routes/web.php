<?php

use App\Http\Controllers\EmployeesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("/search", [EmployeesController::class, 'index']);
Route::post("/search-employee/{id}", [EmployeesController::class, 'searchEmployee']);
Route::get("/employee/{id}", [EmployeesController::class, 'show'])->name('show');
Route::post("/getEmployeesData", [EmployeesController::class, 'getEmployeesData'])->name('getEmployeesData');
Route::get("/reports", [EmployeesController::class, 'reports'])->name('reports');
Route::post('employee/{id}/edit', [EmployeesController::class, 'edit'])->name('edit');
